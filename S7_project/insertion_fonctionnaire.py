import read_data
import db_function
import generateur_class
import sqlite3
import names

# récupération data du nombre de fonctionnaire par catégorie
etpt = read_data.etpt_2021_categorie()

conn = sqlite3.connect('db/environnement.db')
# Enables the foreign key contraints support in SQLite.
conn.execute("PRAGMA foreign_keys = 1")
# Get the cursor for the connection. This object is used to execute queries
# in the database.
cursor = conn.cursor()

categorie_A = int(etpt['A']+etpt['Officiers'])
categorie_B = int(etpt['B'] + etpt['Sous_officiers'])
categorie_C = int(etpt['C'] + etpt['Militaires_rang'])
categories = [('A', categorie_A), ('B', categorie_B), ('C', categorie_C)]

for categorie in categories:
    for _ in range(categorie[1]):

        grade = generateur_class.generateur.random_grade(categorie[0])
        generateur_class.generateur.random_fonctionnaire(grade=grade)

        fonctionnaire = generateur_class.generateur.random_fonctionnaire()
        fonctionnaire.id = db_function.create_matricule(cursor)
        db_function.add_table_test(fonctionnaire.id, names.get_first_name(),
                                   names.get_last_name(), fonctionnaire.grade, fonctionnaire.echelon, fonctionnaire.age, fonctionnaire.nombre_enfant, fonctionnaire.zone_geographique, cursor)
        # Save (commit) the changes
        conn.commit()
