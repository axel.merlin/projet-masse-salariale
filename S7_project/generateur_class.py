from numpy.lib.function_base import quantile
import fonctionnaire_class
import read_data
import random as rd
import pandas as pd
import plotly.express as px
import numpy as np
import copy

structe_armee = read_data.json_structure()
corespondance_indice = read_data.indice_brut_majore()
[corespondance_echelon, grade_ordre, categorie_list] = read_data.carriere_indice()


class generateur:

    # nouveau membre aléatoire uniforme

    def random_fonctionnaire(age=[], echelon=[], grade=[], zone_geographique=[], nombre_enfant=[]):
        if grade == []:
            grade = grade_ordre[rd.randint(0, len(grade_ordre)-1)]
        if echelon == []:
            echelon = generateur.radom_echelon(grade)
        if zone_geographique == []:
            zone_geographique = rd.randint(1, 3)
        if nombre_enfant == []:
            # nombre d'enfants suivant une loi normale de moyenne 1.9 et ecart type 1.3
            nombre_enfant = round(np.random.normal(1.9, 1.3, 1)[0])
        if not(type(age) == int or type(age) == float):
            age = generateur.random_age()
        return fonctionnaire_class.fonctionnaire(grade, echelon, age, zone_geographique=zone_geographique, nombre_enfant=nombre_enfant)

    def radom_echelon(grade):
        return list(corespondance_echelon[grade].keys())[
            rd.randint(0, len(list(corespondance_echelon[grade].keys()))-1)]

    def random_age(retraite=62):
        return rd.randint(18, retraite)

    def random_grade(categorie):
        S = []
        for cle in categorie_list.keys():
            if categorie_list[cle] == categorie:
                S.append(cle)
        return rd.choice(S)

    # nouveau membre aléatoire avec loi exponentielle

    def random_exp_fonctionnaire(self):
        rand_int_exp = round(np.random.exponential(
            len(list(len(grade_ordre)))//2))
        if rand_int_exp >= len(grade_ordre):
            rand_int_exp = len(grade_ordre)-1

        grade = grade_ordre[rand_int_exp]
        rand_int_exp = round(np.random.exponential(
            len(list(corespondance_echelon[grade].keys()))//2))
        if rand_int_exp >= len(list(corespondance_echelon[grade].keys())):
            rand_int_exp = len(list(corespondance_echelon[grade].keys()))-1

        echelon = list(corespondance_echelon[grade].keys())[rand_int_exp]
        age = rd.randint(0, 61)
        return fonctionnaire_class.fonctionnaire(grade, echelon, age, zone_geographique=rd.randint(1, 3), nombre_enfant=round(np.random.normal(1.9, 1.3, 1)))

    # construction unité d'armee/ quantite nombre de composant à partir de élément)

    def structure_builder(element='', quantite=[]):
        dic = copy.deepcopy(structe_armee)
        res = []
        if element == '':
            situation = 0
            effectif = 1
            nb = 1
            while type(dic) == dict:
                while situation < len(quantite):
                    print(quantite[situation], 'creation',
                          dic['denomination'], sep=" ")
                    effectif *= quantite[situation]
                    res.append(generateur.random_fonctionnaire(
                        grade=dic['commandement']))
                    dic = copy.deepcopy(dic['composant'])
                    situation += 1

                if type(dic) == dict:

                    if type(dic["nb_composant_max"]) == int:
                        nb = rd.randint(dic["nb_composant_min"],
                                        dic["nb_composant_max"])
                        print(nb, 'creation', dic['denomination'], sep=" ")
                        effectif *= nb
                    else:
                        nb = round(np.random.exponential(1 /
                                                         dic["nb_composant_min"]))+dic["nb_composant_min"]
                        print(nb, 'creation', dic['denomination'], sep=" ")
                        effectif *= nb

                    dic = copy.deepcopy(dic['composant'])
                    if type(dic) == dict:
                        for _ in range(nb):
                            res.append(generateur.random_fonctionnaire(
                                grade=dic['commandement']))

            print(effectif)
            for _ in range(effectif):
                grade = dic[rd.randint(
                    0, len(dic)-1)]
                res.append(generateur.random_fonctionnaire(grade=grade))
            return res

        if type(element) == str:
            while element != dic["denomination"]:
                dic = copy.deepcopy(dic['composant'])

            situation = 0
            effectif = 1
            while type(dic) == dict:
                while situation < len(quantite):
                    nb = quantite[situation]
                    for _ in range(nb):
                        res.append(generateur.random_fonctionnaire(
                            grade=dic['commandement']))
                    print('creation',
                          dic['denomination'], nb, sep=" ")
                    effectif *= nb

                    dic = copy.deepcopy(dic['composant'])
                    situation += 1

                if type(dic) == dict:

                    if type(dic["nb_composant_max"]) == int:
                        nb = rd.randint(dic["nb_composant_min"],
                                        dic["nb_composant_max"])
                        print('creation', dic['denomination'], nb, sep=" ")
                        effectif *= nb
                    else:
                        nb = round(np.random.exponential(1 /
                                                         dic["nb_composant_min"]))+dic["nb_composant_min"]
                        print('creation', dic['denomination'], nb, sep=" ")
                        effectif *= nb
                    dic = copy.deepcopy(dic['composant'])
                    if type(dic) == dict:
                        for _ in range(nb):
                            res.append(generateur.random_fonctionnaire(
                                grade=dic['commandement']))
            print(effectif)
            for _ in range(effectif):
                grade = dic[rd.randint(
                    0, len(dic)-1)]
                res.append(generateur.random_fonctionnaire(grade=grade))
            return res

        else:
            return 'problème dans la génération de la structure, element non reconnu '


# generateur.structure_builder()
