import csv
import json
from os import terminal_size


def indice_brut_majore():
    corespondance_indice = {}
    with open('data/data_indice.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                line_count += 1
            else:
                corespondance_indice[str(row[0])] = int(row[1])
                line_count += 1
    return corespondance_indice


def carriere_indice():
    corespondance_echelon = {}
    grade_ordre = []
    categorie = {}
    with open('data/echelon_grade_indice_brut.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                line_count += 1
                #print(row)
            else:
                if row[1] == "":
                    data_grade = row[0]
                    grade_ordre.append(data_grade)
                    categorie[data_grade] = row[4]
                    corespondance_echelon[row[0]] = {}
                else:
                    corespondance_echelon[data_grade][int(row[1])] = {
                        'valeur': str(row[2]), 'duree': row[3]}
                line_count += 1
        return [corespondance_echelon, grade_ordre, categorie]


def json_structure():
    # Opening JSON file
    with open('data/structure_armee.json') as json_file:
        data = json.load(json_file)

        # Print the type of data variable
        print("Type:", type(data))
        return data


def etpt_2021_categorie():
    etpt_2021 = {}
    with open('data/etpt_2021_par_categorie.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                line_count += 1
            else:
                etpt_2021[str(row[0])] = float(row[1])
                line_count += 1
    return etpt_2021


def etpt_transfert():
    transfert = []
    transfert_annee = {}
    with open('data/transfert_etpt_par_categorie.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        line_count = 0
        for row in csv_reader:

            if line_count == 0:
                line_count += 1
            else:
                transfert_annee[str(row[0])] = 0
                for k in range(1, len(row)):
                    transfert_annee[str(row[0])] += float(row[k])
                    line_count += 1
        for key in transfert_annee:
            transfert_annee[key] = int(transfert_annee[key])

        categorie_A = transfert_annee['A']+transfert_annee['Officiers']
        categorie_B = transfert_annee['B'] + transfert_annee['Sous_officiers']
        categorie_C = transfert_annee['C'] + transfert_annee['Militaires_rang']
        transfert_final = {'A': categorie_A,
                           'B': categorie_B, 'C': categorie_C}
        transfert.append(transfert_final)
    return transfert




def categorie_fonctionnaire():
    dic={'A':[], 'B':[], 'C':[]}
    for key in carriere_indice()[2].keys():
        if carriere_indice()[2][key]=='A':
            dic['A'].append(key)
        elif carriere_indice()[2][key]=='B':
            dic['B'].append(key)
        else : 
            dic['C'].append(key)
    return dic

print(categorie_fonctionnaire())
# print(etpt_transfert())
#print(carriere_indice()[2])
#print(carriere_indice())