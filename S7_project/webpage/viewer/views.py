from django.shortcuts import render
from django.http import HttpResponse
from viewer.models import Fonctionnaire
# Create your views here.

def hello(request):
    return render(request,'viewer/hello.html')

def tableau(request):
    fonctionnaires = Fonctionnaire.objects.all()

    return render(request,'viewer/data_viz.html',{'fonctionnaires':fonctionnaires})

def grade(request,grades):

    liste_grade = Fonctionnaire.objects.filter(grade=grades)
    return render(request,'viewer/liste_grade.html',{'liste_grade':liste_grade})