# Salaries
Academic project in collaboration with the French State administration (CISIRH). The goal is to provide an intuitive framework to predict the salary mass of the french state in the future.

                            Projet Prévision de la masse salariale pour un fonctionnaire de l'état:
Ce projet vise à prédire le plus fidèlement possible la masse salariale des fonctionnaires de l'Etat français en prenant en compte certaines hypothèses.
Il a été réalisé par Etienne CAPRIOLI, Fahd ET-TAHERY, Louis-Enguerrand AUSSET, Mohammed EL MEJDANI.


**1) Tester le programme:**
Il suffit d'ouvrir le nootebook evolution_v2.ipynb pour pourvoir visualiser l'évolution des salaires d'une popualtion de fonctionnaires. 


**2) Données utilisées:**

Les données utilisées dans ce projet sont toutes issues des sites suivants : 
- fonction-publique.gouv.fr 
- insee.fr
- demarches.interieur.gouv.fr
- service-public.fr
- hopitalex.com
- legifrance.gouv.fr

Vous trouverez une partie des données dans les fichiers grilles_salariales, fonction_publ_etat, fonction_publ_hosp, fonction_publ_territoriale.

**3) Fonctionnement du code:**

Le but est de prédire sur plusieurs années la masse salariale (salaires bruts) d'une population d'agent publics en utilisant un modèle s'appuyant sur une modélisation markovienne (chaine de Markov), et qui prend en compte les paramètres suivants:
- Le nombre d'années de simulations T
- Le nombre d'agents modélisés N
- La valeur du point d'indice p
- Le taux de promotion
- La catégorie des agents (A, B ou C)
- Le versant de la fonction publique (Fonction Publique Territoriale, Hospitalière ou d'Etat)
- La répartition des primes
- Les grilles d'évolutions salariales de la fonction publique

