import db_function
import generateur_class
import sqlite3
import names
import fonctionnaire_class
import read_data
import random as rd
import pandas as pd
import plotly.express as px
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

corespondance_indice = read_data.indice_brut_majore()
[corespondance_echelon, grade_ordre, categorie_list] = read_data.carriere_indice()


class Entreprise_sql:
    def __init__(self):  # fonctionnaires liste de fonctionnaire
        self.age_depart = 62

        # récupération data du nombre de fonctionnaire par catégorie
        etpt = read_data.etpt_2021_categorie()
        self.conn = sqlite3.connect('db/environnement.db')
        # Enables the foreign key contraints support in SQLite.
        self.conn.execute("PRAGMA foreign_keys = 1")
        # Get the cursor for the connection. This object is used to execute queries
        # in the database.
        self.cursor = self.conn.cursor()
        db_function.delete_table(self.cursor)
        self.conn.commit()

        categorie_A = int(etpt['A']+etpt['Officiers'])
        categorie_B = int(etpt['B'] + etpt['Sous_officiers'])
        categorie_C = int(etpt['C'] + etpt['Militaires_rang'])
        categories = [('A', categorie_A), ('B', categorie_B),
                      ('C', categorie_C)]
        # print(categories)
        for categorie in categories:
            for _ in range(categorie[1]//100):  # échantillonnage

                grade = generateur_class.generateur.random_grade(categorie[0])
                generateur_class.generateur.random_fonctionnaire(grade=grade)

                fonctionnaire = generateur_class.generateur.random_fonctionnaire()
                fonctionnaire.id = db_function.create_matricule(self.cursor)
                db_function.add_table_test(fonctionnaire.id, names.get_first_name(),
                                           names.get_last_name(), fonctionnaire.grade, fonctionnaire.echelon, fonctionnaire.age, fonctionnaire.nombre_enfant, fonctionnaire.zone_geographique, self.cursor)
                # Save (commit) the changes
                self.conn.commit()
        # tableau vec transfert par annee et par categorie faire le prétraitement
        self.transfert = read_data.etpt_transfert()
        print('fin de l\'initialisation de l\'an 0')

    def ajout(self, fonctionnaire_add):
        fonctionnaire_add.id = db_function.create_matricule(self.cursor)
        db_function.add_table_test(fonctionnaire_add.id, names.get_first_name(),
                                   names.get_last_name(), fonctionnaire_add.grade, fonctionnaire_add.echelon, fonctionnaire_add.age, fonctionnaire_add.nombre_enfant, fonctionnaire_add.zone_geographique, self.cursor)
        self.conn.commit()

    def quit(self, fonctionnaire_quitte):
        db_function.delete_fonctionnaire(fonctionnaire_quitte.id, self.cursor)
        self.conn.commit()

    def mise_a_jour_sql(self, fonctionnaire):
        db_function.update(fonctionnaire.id, fonctionnaire.grade, fonctionnaire.echelon, fonctionnaire.age,
                           fonctionnaire.nombre_enfant, fonctionnaire.zone_geographique, self.cursor)
        self.conn.commit()

    def evolution(self, delta_time):
        if len(self.transfert) > 0:
            transfert_annee = self.transfert.pop(0)
            if len(self.transfert) == 0:
                print('fin des prévisions\n')
        else:
            print("prévision sans ajout de fonctionnaire")
            transfert_annee = {'A': 0, 'B': 0, 'C': 0}
        fonctionnaires_param = db_function.get_table_fonctionnaire(self.cursor)
        for fonctionnaire_param in fonctionnaires_param:
            fonctionnaire = fonctionnaire_class.fonctionnaire(fonctionnaire_param[3], fonctionnaire_param[4], age=fonctionnaire_param[
                                                              5], nombre_enfant=fonctionnaire_param[6], zone_geographique=fonctionnaire_param[7], id=fonctionnaire_param[0])
            fonctionnaire.viellissement(delta_time)
            if fonctionnaire.age > self.age_depart:
                transfert_annee[fonctionnaire.categorie] -= 1
                self.quit(fonctionnaire)
            # print(fonctionnaire.grade, fonctionnaire.echelon)
            self.mise_a_jour_sql(fonctionnaire)
        for categorie in transfert_annee:
            if transfert_annee[categorie] > 0:
                for _ in range(transfert_annee[categorie]//100):
                    # ajout de fonctionnaires
                    grade = generateur_class.generateur.random_grade(categorie)
                    generateur_class.generateur.random_fonctionnaire(
                        grade=grade)

                    fonctionnaire = generateur_class.generateur.random_fonctionnaire()
                    fonctionnaire.id = db_function.create_matricule(
                        self.cursor)
                    db_function.add_table_test(fonctionnaire.id, names.get_first_name(),
                                               names.get_last_name(), fonctionnaire.grade, fonctionnaire.echelon, fonctionnaire.age, fonctionnaire.nombre_enfant, fonctionnaire.zone_geographique, self.cursor)
                    # Save (commit) the changes
                    self.conn.commit()
            if transfert_annee[categorie] < 0:
                # retiré des fonctionnaires
                rd_list = []
                for grade in categorie_list:
                    if categorie_list[grade] == categorie:
                        rd_list.append(grade)

                for _ in range(transfert_annee[categorie]//100):
                    rd_grade = rd_list[rd.randint(0, len(rd_list)-1)]
                    id_select = db_function.select_grade_rand(
                        self.cursor, rd_grade)
                    db_function.delete_fonctionnaire(id_select, self.cursor)
                    self.conn.commit()
                print(0)

    def masse_salariale(self):
        fonctionnaires = db_function.get_table_fonctionnaire(self.cursor)

        res = 0
        for fonctionnaire in fonctionnaires:
            fonctionnaireclass = fonctionnaire_class.fonctionnaire(
                fonctionnaire[3], fonctionnaire[4], age=fonctionnaire[5], nombre_enfant=fonctionnaire[6], zone_geographique=fonctionnaire[7], id=fonctionnaire[0])
            res += fonctionnaireclass.salaire
        return res

    def mesure(self):  # retourne un dictionnaire contenant le salaire moyenne par mois d'une personne de catégorie A,B et C
        categorie = read_data.categorie_fonctionnaire()
        dic_salaires = {}
        for key in categorie.keys():
            salaire = 0
            tmp = db_function.select_fonctionnaire_by_categorie(
                self.cursor, categorie[key])
            for fonctionnaire in tmp:
                salaire += fonctionnaire_class.fonctionnaire(fonctionnaire[3], fonctionnaire[4], age=fonctionnaire[5],
                                                             nombre_enfant=fonctionnaire[6], zone_geographique=fonctionnaire[7], id=fonctionnaire[0]).salaire
            salaire_moy = salaire/len(tmp)
            dic_salaires[key] = {
                'salaire_moy': salaire_moy, 'masse_salariale': salaire*12}
        return dic_salaires

    def repartition_age(self):
        age_list = list(range(18, 64, 5))
        print(age_list)

        mesh_value = np.zeros((len(age_list)-1, len(grade_ordre)))
        for i in range(len(grade_ordre)):
            for j in range(len(age_list)-1):
                count_age = db_function.count_age_grade(
                    self.cursor, grade_ordre[i], age_list[j], age_list[j+1])
                mesh_value[j][i] = count_age

        ylabel = []
        for k in range(len(age_list)-1):
            ylabel.append("["+str(age_list[k])+","+str(age_list[k+1])+"[")
        fig, ax = plt.subplots()
        sns.heatmap(mesh_value, xticklabels=grade_ordre, yticklabels=ylabel)
        plt.show()


def simu(end_annee):  # hypothèse effectif stable: entrées=sorties et n le nombre de fonctionnaire
    entreprise = Entreprise_sql()

    # initialisation objet panda pour les graphiques
    annee = 0
    historique = pd.DataFrame(columns=['grade', 'nombre', 'annee'])
    masse_salariale = pd.DataFrame(columns=['masse_salariale', 'annee'])
    nb_par_grade = {}  # compte de fonctionnaire par type de grade
    for grade in grade_ordre:
        nb_par_grade[grade] = db_function.count_grade(
            entreprise.cursor, grade)

    for grade in nb_par_grade:  # ajout dans les objet pandas du compte effectué
        historique = historique.append(
            {'grade': grade, 'nombre': nb_par_grade[grade], 'annee': annee}, ignore_index=True)
    masse_salariale = masse_salariale.append(
        {'masse_salariale': entreprise.masse_salariale(), 'annee': annee}, ignore_index=True)
    print(entreprise.masse_salariale())

    annee = 1
    while annee <= end_annee:  # début simulation

        entreprise.evolution(1)  # veillissement de l'entreprise

        nb_par_grade = {}  # compte de fonctionnaire par type de grade
        for grade in grade_ordre:
            nb_par_grade[grade] = db_function.count_grade(
                entreprise.cursor, grade)

        for grade in nb_par_grade:  # ajout dans les objet pandas du compte effectué
            historique = historique.append(
                {'grade': grade, 'nombre': nb_par_grade[grade], 'annee': annee}, ignore_index=True)
        masse_salariale = masse_salariale.append(
            {'masse_salariale': entreprise.masse_salariale(), 'annee': annee}, ignore_index=True)
        print(entreprise.masse_salariale())
        annee += 1

    print(historique.head())
    n = len(db_function.get_table_fonctionnaire(entreprise.cursor))
    fig_fonctionnaire = px.bar(historique, x="grade", y="nombre", color="grade",
                               animation_frame="annee", range_y=[0, int(n/5)])
    fig_masse_salariale = px.line(
        masse_salariale, y="masse_salariale", x="annee")
    fig_fonctionnaire.show()
    fig_masse_salariale.show()


def simu_mesure():
    entreprise = Entreprise_sql()
    print(entreprise.mesure())
    entreprise.evolution(1)
    print(entreprise.mesure())


# simu(1)
simu_mesure()
#entreprise = Entreprise_sql()
# entreprise.repartition_age()
