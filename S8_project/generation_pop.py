import numpy as np
import random
from scipy.stats import expon
import matplotlib.pyplot as plt

from utilities import list_expo


def generer_pop(N):  # retourne une liste de N vecteurs
    # chaque vecteur contient type de Fp (état, territoriale, hospitalière)
    #catégorie (A,B ou C), indice brut majoré, maladie, age, nombre d'enfants à charge,
    # zone géographique (1,2 ou 3), nbi

    L = [0 for i in range(N)]

    fpub_lst = [1, 2,
                3]  # 1:Fp de l'état, 2:Fp territoriale, 3:Fp hospitalière
    fpub = random.choices(fpub_lst, weights=(44, 35, 21), k=N)

    cat_lst = [1, 2, 3]  # 1:A, 2:B, 3:C
    categorie_etat = random.choices(cat_lst, weights=(54.6, 23.5, 20.9), k=N)
    categorie_terri = random.choices(cat_lst, weights=(12.4, 11.9, 74.7), k=N)
    categorie_hosp = random.choices(cat_lst, weights=(39.4, 12.3, 48.3), k=N)
    categorie = [0 for i in range(N)]
    for i in range(N):
        if fpub[i] == 1:
            categorie[i] = categorie_etat[i]
        elif fpub[i] == 2:
            categorie[i] = categorie_terri[i]
        else:
            categorie[i] = categorie_hosp[i]

    # lam4 = 1/(400)
    idm_lst = range(203, 831)
    # idm = random.choices(idm_lst, weights=[np.exp(-lam4*(i-203))-np.exp(-lam4*(i+1-203)) for i in range(203,831)], k=N)

    age_lst = [1, 2, 3]
    age = random.choices(age_lst, weights=(14.3, 50.8, 34.9), k=N)
    for i in range(N):
        if age[i] == 1:
            age[i] = random.randrange(15, 30)
        elif age[i] == 2:
            age[i] = age[i] = random.randrange(30, 50)
        else:
            age[i] = age[i] = random.randrange(50, 65)

    ##IM attribué par régression linéaire
    def IM(age):
        a = 300 / 52
        b = 300 - 16 * a
        return a * age + b

    sigma = 80
    idm = IM(np.array(age)) + np.array(
        [random.gauss(0, sigma) for i in range(N)])
    idm = [int(k) for k in idm]


    jour_malad = [0 for i in range(N)]
    jour_malad_lst = [0, 4, 11.5, 22.5, 30]
    jour_malad_1 = random.choices(jour_malad_lst,
                                  weights=(70, 19.2, 4.5, 1.2, 5.4),
                                  k=N)
    jour_malad_2 = random.choices(jour_malad_lst,
                                  weights=(62, 20.9, 6.08, 3.04, 6.3),
                                  k=N)
    jour_malad_3 = random.choices(jour_malad_lst,
                                  weights=(64, 15.48, 7.92, 2.16, 10.44),
                                  k=N)
    for i in range(N):
        if age[i] == 1:
            jour_malad[i] = jour_malad_1[i]
        elif age[i] == 2:
            jour_malad[i] = jour_malad_2[i]
        else:
            jour_malad[i] = jour_malad_3[i]

    sexe_lst = [1, 2]  #1:F, 2:H
    sexe = random.choices(sexe_lst, weights=(57, 43), k=N)

    nbre_enf_lst = [0, 1, 2, 3, 4
                    ]  # 0: sans enfants, 1: 1 enfant,..., 4: 4 enfants ou plus
    nbre_enf = []
    for i in range(N):
        if sexe[i] == 1:
            r = random.choices(nbre_enf_lst,
                               weights=(46.7, 24.0916, 20.4139, 6.8224,
                                        1.9721),
                               k=1)
            nbre_enf.append(r[0])
        else:
            r = random.choices(nbre_enf_lst,
                               weights=(40.7, 26.836, 22.7119, 7.5904, 2.1941),
                               k=1)
            nbre_enf.append(r[0])

    zone_lst = [1, 2, 3]
    zone = random.choices(zone_lst, weights=(39.2, 27.25, 33.55), k=N)

    lam1 = 1 / (50)
    lam2 = 1 / (15)
    lam3 = 1 / (13)

    nbi = [0 for i in range(N)]
    nbi_A_lst = range(15, 121)
    nbi_B_lst = range(10, 31)
    nbi_C_lst = range(10, 21)
    nbi_A = random.choices(nbi_A_lst,
                           weights=[
                               np.exp(-lam1 * (i - 15)) - np.exp(-lam1 *
                                                                 (i + 1 - 15))
                               for i in range(15, 121)
                           ],
                           k=N)
    nbi_B = random.choices(nbi_B_lst,
                           weights=[
                               np.exp(-lam2 * (i - 10)) - np.exp(-lam2 *
                                                                 (i + 1 - 10))
                               for i in range(10, 31)
                           ],
                           k=N)
    nbi_C = random.choices(nbi_C_lst,
                           weights=[
                               np.exp(-lam3 * (i - 10)) - np.exp(-lam3 *
                                                                 (i + 1 - 10))
                               for i in range(10, 21)
                           ],
                           k=N)
    for i in range(N):
        if categorie[i] == 1:
            nbi[i] = nbi_A[i]
        elif categorie[i] == 2:
            nbi[i] = nbi_B[i]
        else:
            nbi[i] = nbi_C[i]

    for i in range(N):
        L[i] = np.zeros(8)
        L[i][0] = fpub[i]
        L[i][1] = categorie[i]
        L[i][2] = idm[i]
        L[i][3] = jour_malad[i]
        L[i][4] = age[i]
        L[i][5] = nbre_enf[i]
        L[i][6] = zone[i]
        L[i][7] = nbi[i]
    return L


# L = np.array(generer_pop(2000))
# print(L)
# plt.figure()
# plt.scatter(L[:, 4], L[:, 2]) # IM en fonction de l'age
# plt.show()
