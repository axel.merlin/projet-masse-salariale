from django.db import models
from pathlib import Path
import sqlite3
from django.db.models.base import Model
from django.db.models.expressions import Col
import pandas as pd
from pandas.core.algorithms import mode
from django.core.validators import MaxValueValidator , MinValueValidator

db_folder = Path("/home/varda/masse_salariale/projet-masse-salariale/db")

file_to_open = db_folder / "environnement.db"

conn = sqlite3.connect(file_to_open)
conn.execute("PRAGMA foreign_keys = 1")

cursor = conn.cursor()
pandas_db = pd.read_sql('SELECT * FROM Fonctionnaire',conn)

cursor.close()
conn.commit()

conn.close()

Column_names = list(pandas_db.columns)

grades = list(pandas_db['grade'].drop_duplicates())
# print(Column_names)
TEMP_CHOICES = ()
for i in range(len(grades)):
    TEMP_CHOICES = TEMP_CHOICES + ((grades[i],grades[i]), )

# Create your models here.
# pandas_db.to_json(path_or_buf="/home/varda/masse_salariale/projet-masse-salariale/webpage/db.json")
class Fonctionnaire(models.Model):
    identity = models.IntegerField(max_length=11, primary_key=True)
    name = models.fields.CharField(max_length=100)
    surname = models.fields.CharField(max_length=100)
    grade = models.fields.CharField(max_length=100,choices = TEMP_CHOICES,default='colonel')
    echelon = models.fields.IntegerField(default=1)
    age = models.fields.IntegerField(default=30)
    nombre_enfant = models.fields.IntegerField(default=0)
    Zone_geographique = models.fields.IntegerField(default=1)

# print(len(Fonctionnaire.objects.all()))

filled = False

def fill_db(pandas_db,filled):
    if not filled:
        for i in range(len(pandas_db)):
            fonctionnaire = Fonctionnaire()
            fonctionnaire.identity = pandas_db['id'][i]
            fonctionnaire.name = pandas_db['name'][i]
            fonctionnaire.surname = pandas_db['surname'][i]
            fonctionnaire.grade = pandas_db['grade'][i]
            fonctionnaire.echelon = pandas_db['echelon'][i]
            fonctionnaire.age = pandas_db['age'][i]
            fonctionnaire.nombre_enfant = pandas_db['nombre_enfant'][i]
            fonctionnaire.Zone_geographique = pandas_db['zone_geographique'][i]
            fonctionnaire.save()
        filled = True

fill_db(pandas_db,filled)