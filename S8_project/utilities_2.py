from math import comb
import numpy as np


def list_binom(p, n, eps=1e-30):
    '''Renvoie une liste de taille n+1 avec les n valeurs de la fonction de masse (P(X=i)) 
    de la loi binomiale de paramètre (n, p)
    
    Forme du résultat : [P(X=1), P(X=2), ...., P(X=n)]'''
    n -= 1
    q = 1 - p
    L = []
    res = 0
    for i in range(n + 1):
        a = comb(n, i) * (p**i) * (q**(n - i))
        if a > eps:
            L.append(comb(n, i) * (p**i) * (q**(n - i)))
        else:
            L.append(a)
            res += 0
    L = np.array(L)
    i = np.argmax(L)
    L[i] += res
    return L

