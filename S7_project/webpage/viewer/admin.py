from django.contrib import admin

from viewer.models import Fonctionnaire

class FonctionnaireAdmin(admin.ModelAdmin):
    list_display = ('name','echelon','grade','age')
admin.site.register(Fonctionnaire,FonctionnaireAdmin)
# Register your models here.
