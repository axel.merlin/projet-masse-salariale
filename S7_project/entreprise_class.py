import fonctionnaire_class
import read_data
import generateur_class
import random as rd
import pandas as pd
import plotly.express as px
import numpy as np

corespondance_indice = read_data.indice_brut_majore()
[corespondance_echelon, grade_ordre, categorie_list] = read_data.carriere_indice()


class Entreprise:
    def __init__(self, fonctionnaires):  # fonctionnaires liste de fonctionnaire
        self.age_depart = 62
        if len(fonctionnaires) <= 0:
            raise NameError('Entreprise vide')
        else:
            self.nb_fonctionnaire = len(fonctionnaires)
            self.fonctionnaires = fonctionnaires

    def ajout(self, fonctionnaire_add):
        self.nb_fonctionnaire += 1
        self.fonctionnaires.append(fonctionnaire_add)

    def quit(self, fonctionnaire_quitte):
        if self.nb_fonctionnaire == 1:
            print('l\'entreprise a déposé le bilan')
            self.nb_fonctionnaire = 0
        else:
            self.nb_fonctionnaire -= 1
            del self.fonctionnaires[self.fonctionnaires.index(
                fonctionnaire_quitte)]

    def evolution(self, delta_time):
        for fonctionnaire in self.fonctionnaires:
            fonctionnaire.viellissement(delta_time)
            if fonctionnaire.age > self.age_depart:
                self.quit(fonctionnaire)
            print(fonctionnaire.grade, fonctionnaire.echelon)

    def masse_salariale(self):
        res = 0
        for fonctionnaire in self.fonctionnaires:
            res += fonctionnaire.salaire
        return res

    def random_fonctionnaire(self):  # nouveau membre aléatoire uniforme
        grade = grade_ordre[rd.randint(0, len(grade_ordre)-1)]
        echelon = list(corespondance_echelon[grade].keys())[
            rd.randint(0, len(list(corespondance_echelon[grade].keys()))-1)]
        age = rd.randint(0, 61)
        return fonctionnaire_class.fonctionnaire(grade, echelon, age)

    # nouveau membre aléatoire avec loi exponentielle
    def random__exp_fonctionnaire(self):
        rand_int_exp = round(np.random.exponential(
            len(list(len(grade_ordre)))//2))
        if rand_int_exp >= len(grade_ordre):
            rand_int_exp = len(grade_ordre)-1

        grade = grade_ordre[rand_int_exp]
        rand_int_exp = round(np.random.exponential(
            len(list(corespondance_echelon[grade].keys()))//2))
        if rand_int_exp >= len(list(corespondance_echelon[grade].keys())):
            rand_int_exp = len(list(corespondance_echelon[grade].keys()))-1

        echelon = list(corespondance_echelon[grade].keys())[rand_int_exp]
        age = rd.randint(0, 61)
        return fonctionnaire_class.fonctionnaire(grade, echelon, age)
# générer des fonctionnaires avec la loi exponentielle


def simu(n, end_annee):  # hypothèse effectif stable: entrées=sorties et n le nombre de fonctionnaire
    fonctionnaires = []  # initialisation de l'entreprise
    for _ in range(n):
        fonctionnaires.append(
            generateur_class.generateur.random_fonctionnaire(grade="general", echelon=3))

    entreprise = Entreprise(fonctionnaires)
    annee = 0
    # initialisation objet panda pour les graphiques
    historique = pd.DataFrame(columns=['grade', 'nombre', 'annee'])
    masse_salariale = pd.DataFrame(columns=['masse_salariale', 'annee'])
    while annee < end_annee:  # début simulation

        entreprise.evolution(1)  # veillissement de l'entreprise
        if entreprise.nb_fonctionnaire < n:  # entreprise avec un nombre de fonctionnaire stable
            entreprise.ajout(
                generateur_class.generateur.random_fonctionnaire())

        nb_par_grade = {}  # compte de fonctionnaire par type de grade
        for fonctionnaire in entreprise.fonctionnaires:
            if fonctionnaire.grade in nb_par_grade:
                nb_par_grade[fonctionnaire.grade] += 1
            else:
                # initialisation à 1 du grade dans le dictionnaire
                nb_par_grade[fonctionnaire.grade] = 1

        for grade in nb_par_grade:  # ajout dans les objet pandas du compte effectué
            historique = historique.append(
                {'grade': grade, 'nombre': nb_par_grade[grade], 'annee': annee}, ignore_index=True)
        masse_salariale = masse_salariale.append(
            {'masse_salariale': entreprise.masse_salariale(), 'annee': annee}, ignore_index=True)
        print(entreprise.masse_salariale())
        annee += 1

    print(historique.head())
    fig_fonctionnaire = px.bar(historique, x="grade", y="nombre", color="grade",
                               animation_frame="annee", range_y=[0, int(n/5)])
    fig_masse_salariale = px.line(
        masse_salariale, y="masse_salariale", x="annee")
    fig_fonctionnaire.show()
    fig_masse_salariale.show()


simu(10, 50)
