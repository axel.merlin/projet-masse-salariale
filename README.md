# Data

## data_indice.csv

Fichier csv contenant la correspondance entre les indices bruts et les indices majorés

## echelon_grade_indice_brut.csv

Fichier csv contenant la correspondance entre les (grade, échelon) et les indices brut. Ce fichier contient également les informations de durées nécessaire pour passer d'un échelon à l'autre.

A noter: lorsque les durées ne sont pas précisés, cela signifie que le fonctionnaire à la possiblité de rester à cet échelon

## remuneration.json

Fichier json contenant les coûts par an par catégorie des fonctionnaires.

## structure_armee.json

Fichier json décrivant la structure de l'administration militaire

## transfert_etpt_par_categorie.csv

Fichier csv contenant les transfert par catégorie.

# Programme python

## fonctionnaire_class.py

Ce fichier construit une classe fonctionnaire prenant en compte divers éléments pour calculer le salaire d'un fonctionnaire.
Information prise en compte:

- le grade
- l'échelon
- le nombre d'enfant
- le corps
- la catégorie

La classe fonctionnaire possède plusieurs méthodes:

- salaire_base: calcule le salaire dû au fonctionnaire pour le travail réaliser en fonction de la valeur du point
- supplement_enfant: calcule le supplement sur le salaire pour le nombre d'enfant

- supplement_ir: calcule le supplement sur le salaire pour le lieu de résidence
- get_salaire: retourne le salaire final du fonctionnaire

ATTENTION: le salaire est donné en brut par mois

## entreprise_class.py & entreprise_class2.py

Fichier python permettant de lancer la simulation de l'administration. entreprise_class.py est une version stockant les fonctionnaires directement sur la mémoire de notre ordinateur. entreprise_class2.py est une version de la classe entreprise qui stocke les fonctionnaires sur une data base, ce qui permet de traiter un plus grand nombre de fonctionnaires.

## generateur_class.py

Fichier python permettant de créer un ou des fonctionnaires suivant des critères précis tels que l'organisation simulée ou des informations sur les fonctionnaires.

## db.py

Initialise la base de donnée

## db_funtion.py

Définit les requêtes SQL

## insertion_fonctionnaire.py

Insère un ensemble de fonctionnaire dans la base de donnée manuellement

# db

Dossier contenant la database.

# Masse_salariale

## urls.py

Ce fichier recueille l'adresse URL de la requête et la compare avec les adresses enregistrées. Si elle est enregistrée la requête est redirigée vers display.urls. Sinon un message d'erreur est renvoyé.

# Application viewer

Viewer, intégralement développer en Django, permet de créer une interface graphique permettant d'afficher des données utiles par exemple
avoir la liste des soldats.

## models.py

Ce fichier construit la table fonctionnaire dont les colonnes fournissent les différents éléments utiles aux calculs du salaire et à la reconnaissance du fonctionnaire:

- nom
- age
- nombre d'enfants
- la zone_géogarphique
- le corps
- le grade
- échelon

Chaque fois que le serveur est lancé, il met à jour la base de donnée avant de charger les différentes urls.

## urls.py

Ce module est la plateforme où transite les liens urls. Chque fois que les urls sont chargés, django est redirigé sur ce fichier pour ensuite
compare l'url avec les urls enregistrées. Si il y a correspondance la réponse assurée par viewer.views. Sinon un message d'erreur est retourné.

## views.py

La "request" est envoyé sur une des fonctions de views.py. Les fonctions définissent les données à traiter ainsi qu'un fichier html qui créera le squelette du site web et discriminera les données.

## templates

contient les ficiers html qui sont les squelettes des pages web

## admin

C'est possible de créer une interface administrateur pour gérer la base de donnée. Et pour la créer aller à la section Commandes utiles.

# commandes utiles

## py manage.py runserver (windows)

Permet de lancer le serveur web, met en liaison le frontend et le backend.

## Quelques adresses urls

-http://127.0.0.1:8000/display/ -http://127.0.0.1:8000/display/grades/ (grades=sergent, lieutenant,...)
