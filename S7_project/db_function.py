import sqlite3
import names
import generateur_class
# Code for an unexpected error in the database.
UNEXPECTED_ERROR = -1

# Code for a duplicate registration error.
DUPLICATE_REGISTRATION_ERROR = 0


def get_table_fonctionnaire(cursor):
    """Returns the Pistus edition on the specified year.

    Parameters
    ----------
    table_name : string
    cursor :
        The object used to query the database.

    Return
    ------
    contenu de la table
    If an error occurs while reading the database, the function returns None.
    """
    try:
        res = []
        data = cursor.execute("SELECT * FROM Fonctionnaire ORDER BY id DESC")
        for row in data:
            res.append(row)
        return res
    except sqlite3.Error as error:
        print(error)
        return None


def count_grade(cursor, grade):
    try:
        count = cursor.execute(
            "SELECT count(*) FROM Fonctionnaire WHERE grade=?", (grade, ))
        return list(count)[0][0]
    except sqlite3.Error as error:
        print(error)
        return None


def select_grade_rand(cursor, grade):
    try:
        id_select = cursor.execute(
            "SELECT id FROM Fonctionnaire WHERE grade=? ORDER BY RAND()", (grade, ))
        return list(id_select)[0][0]
    except sqlite3.Error as error:
        print(error)
        return None


def count_age_grade(cursor, grade, age_min, age_max):
    try:
        count_age = cursor.execute(
            "SELECT count(id) FROM Fonctionnaire WHERE grade=? AND age>=? AND age<? ", (grade, age_min, age_max))
        return list(count_age)[0][0]
    except sqlite3.Error as error:
        print(error)
        return None


def add_table_test(id, name, surname, grade, echelon, age, nombre_enfant, zone_geographique, cursor):
    try:
        cursor.execute("INSERT INTO Fonctionnaire VALUES(?,?,?,?,?,?,?,?)",
                       (id, name, surname, grade, echelon, age, nombre_enfant, zone_geographique))
    except sqlite3.Error as error:
        print("An error occurred while adding the Test : {}".format(error))
        return (False, UNEXPECTED_ERROR, error)
    return (True, None, None)


def update(id, grade, echelon, age, nombre_enfant, zone_geographique, cursor):
    try:
        cursor.execute("UPDATE Fonctionnaire SET grade=?,echelon=?, age=?, nombre_enfant=?,zone_geographique=? WHERE id=?",
                       (grade, echelon, age, nombre_enfant, zone_geographique, id))
    except sqlite3.Error as error:
        print("An error occurred while adding the Test : {}".format(error))
        return (False, UNEXPECTED_ERROR, error)
    return (True, None, None)


def create_matricule(cursor):
    id_search_field = get_table_fonctionnaire(cursor)
    if len(id_search_field) == 0:
        return 1
    else:
        return id_search_field[0][0]+1


def delete_fonctionnaire(id, cursor):
    try:
        cursor.execute("DELETE FROM Fonctionnaire WHERE id = ?", (id, ))
    except sqlite3.Error as error:
        print("An error occurred while adding the Test : {}".format(error))
        return (False, UNEXPECTED_ERROR, error)
    return (True, None, None)


def delete_table(cursor):
    try:
        cursor.execute("DELETE FROM Fonctionnaire")
    except sqlite3.Error as error:
        print("An error occurred while adding the Test : {}".format(error))
        return (False, UNEXPECTED_ERROR, error)
    return (True, None, None)


def select_fonctionnaire_by_categorie(cursor, categorie):
    try:
        categorie_tuple = tuple(categorie)
        for grade in categorie_tuple:
            id_select = cursor.execute(
                'SELECT * FROM Fonctionnaire WHERE grade = ?', (grade, ))
        return list(id_select)
    except sqlite3.Error as error:
        print(error)
        return None


conn = sqlite3.connect('db/environnement.db')
# Enables the foreign key contraints support in SQLite.
conn.execute("PRAGMA foreign_keys = 1")
# Get the cursor for the connection. This object is used to execute queries
# in the database.
cursor = conn.cursor()
fonctionnaire = generateur_class.generateur.random_fonctionnaire()
add_table_test(create_matricule(cursor), names.get_first_name(),
               names.get_last_name(), fonctionnaire.grade, fonctionnaire.echelon, fonctionnaire.age, fonctionnaire.nombre_enfant, fonctionnaire.zone_geographique, cursor)
# Save (commit) the changes
conn.commit()
get_table_fonctionnaire(cursor)
