from django.urls import path
from . import views

urlpatterns = [ path('',views.tableau,name='tableau'),
                path('<str:grades>/',views.grade,name='grades')
]