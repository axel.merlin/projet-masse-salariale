# Generated by Django 3.2.6 on 2022-01-20 09:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('viewer', '0008_remove_fonctionnaire_identity'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='fonctionnaire',
            name='id',
        ),
        migrations.AddField(
            model_name='fonctionnaire',
            name='identity',
            field=models.IntegerField(default=1, primary_key=True, serialize=False),
        ),
    ]
