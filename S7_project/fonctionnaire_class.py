import numpy as np
import read_data


corespondance_indice = read_data.indice_brut_majore()
[corespondance_echelon, grade_ordre, categorie_list] = read_data.carriere_indice()


class fonctionnaire:
    def __init__(self, grade, echelon, age, nombre_enfant=0, zone_geographique=3, corps="armee de terre", carriere=0, id=""):
        self.nombre_enfant = nombre_enfant
        self.age = age
        self.zone_geographique = zone_geographique
        self.corps = corps
        self.grade = grade
        self.categorie = categorie_list[grade]
        self.carriere = carriere  # temps passé dans l'échelon en année
        self.echelon = echelon
        self.indice_brut = corespondance_echelon[grade][echelon]['valeur']
        self.salaire = self.salaire_base()+self.supplement_familial()+self.supplement_ir()
        self.id = id

    def salaire_base(self, valeur_point=4.686025):
        return valeur_point*corespondance_indice[self.indice_brut]

    def supplement_familial(self):  # indemminité familiale
        supplement = 0
        if self.nombre_enfant == 0:
            return supplement
        else:
            if self.nombre_enfant == 1:
                supplement += 2.29
            if self.nombre_enfant == 2:
                supplement += 10.67+0.03*self.salaire_base()
            if self.nombre_enfant == 3:
                supplement += 15.24+0.06*self.salaire_base()
            else:
                supplement += self.nombre_enfant*4.57+self.nombre_enfant*0.06*self.salaire_base()
            return supplement

    def supplement_ir(self):  # indemminté de résidence
        if self.zone_geographique == 1:
            return self.salaire_base()*0.03
        if self.zone_geographique == 2:
            return self.salaire_base()*0.01
        else:
            return 0

    def get_salaire(self):  # donne le salaire au centième près
        return round(self.salaire, 2)

    def viellissement(self, delta_time):  # delta_time en année
        #print(self.grade, self.echelon)
        nb_echelon = len(list(corespondance_echelon[self.grade].keys()))
        if self.echelon < nb_echelon:  # pas de changement de grade
            # échelon à durée indeterminée
            if corespondance_echelon[self.grade][self.echelon]['duree'] == '':
                if np.random.rand() <= (self.echelon)/nb_echelon:  # passage en aléatoire à l'échelon suivant
                    self.carriere += delta_time
                    self.age += delta_time
                else:
                    self.carriere = 0  # passage en aléatoire à l'échelon suivant
                    self.echelon += 1
                    self.age += delta_time
            else:
                if self.carriere < float(corespondance_echelon[self.grade][self.echelon]['duree']):
                    self.carriere += delta_time
                    self.age += delta_time
                else:  # passage à l'échelon suivant avec durée déterminée
                    self.carriere = 0
                    self.echelon += 1
                    self.age += delta_time

        if self.echelon == nb_echelon:  # changement de grade possible:
            if corespondance_echelon[self.grade][self.echelon]['duree'] != '':
                if self.carriere < float(corespondance_echelon[self.grade][self.echelon]['duree']):
                    self.carriere += delta_time
                    self.age += delta_time
                else:
                    self.grade = grade_ordre[grade_ordre.index(self.grade)+1]
                    self.echelon = 1
                    self.carriere = 0
                    self.age += delta_time

            else:
                if np.random.rand() <= (grade_ordre.index(self.grade)+1)/len(grade_ordre):
                    self.carriere += delta_time
                    self.age += delta_time
                else:  # changement de grade
                    if grade_ordre.index(self.grade) < len(grade_ordre):
                        self.grade = grade_ordre[grade_ordre.index(
                            self.grade)+1]
                        self.echelon = 1
                        self.carriere = 0
                        self.age += delta_time
                    else:  # grade max et echelon max atteint
                        self.age += delta_time
                        self.carriere += delta_time

        self.indice_brut = corespondance_echelon[self.grade][self.echelon]['valeur']
        self.salaire = self.salaire_base()+self.supplement_familial()+self.supplement_ir()


def test():
    fonctionnaire1 = fonctionnaire('soldat 2', 1, 0)
    print(fonctionnaire1.salaire)
    print(fonctionnaire1.get_salaire())
    print(fonctionnaire1.indice_brut)
    while fonctionnaire1.grade != "general":
        fonctionnaire1.viellissement(1)
        print(fonctionnaire1.grade)
        print(fonctionnaire1.echelon)
        print(fonctionnaire1.get_salaire())
    print(fonctionnaire1.age)

    # print(grade_ordre)
    # print(corespondance_echelon)
    # print(corespondance_indice)


test()
