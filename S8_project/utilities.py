import numpy as np


def list_geom(p, n):
    '''Renvoie une liste de taille n+1 avec les n premières valeurs de la fonction de masse (P(X=i)) 
    de la loi géométrique de paramètre p et donne la probabilité restante (1-sum(n primères proba)) en position n+1
    
    Forme du résultat : [P(X=1), P(X=2), ...., P(X=n), 1 - (P(X=1) + P(X=2) + ... + P(X=n))]'''

    q = 1 - p
    L = []
    for i in range(n):
        L.append(p * (q**i))
    # L.append(0)
    L = np.array(L)
    #L[n] = 1-np.sum(L)
    L[0] += 1 - np.sum(L)
    return L


def tirage_al(liste):
    '''list : liste/array dont les éléments somment à 1,
     représente une distribution de probabilité où la probabilité d'obtenir la classe de l'index i est l'élément L[i]
     
     La fonction renvoie un indexe, tiré au hasard en siuvant la distribution décrite par list'''

    s = np.cumsum(
        np.array(liste))  #réalise la somme cumulée des coefficients de list
    nbr = np.random.random()
    l = 0
    while s[l] <= nbr:
        l += 1
    return l


def list_expo(lam, n):
    '''Renvoie une liste de taille n+1 avec les n premières valeurs de la fonction de masse (P(X=i)) 
    d'une version discrétisée de la loi exponentielle de paramètre lambda (normalisée)
    
    Forme du résultat : [P(X=1), P(X=2), ...., P(X=n)]'''

    x = np.array(range(n))
    y1 = lam * np.exp(-lam * x)
    y1 = y1 / y1.sum()

    return list(y1)
